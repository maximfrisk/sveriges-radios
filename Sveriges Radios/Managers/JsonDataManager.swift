//
//  JsonDataManager.swift
//  Sveriges Radios
//
//  Created by Maxim Frisk on 2018-11-10.
//  Copyright © 2018 Maxim Frisk. All rights reserved.
//

import UIKit


public protocol JsonDataManagerDelegate : AnyObject {
    
    func jsonDataRequestDidFinish(_ dataManager: JsonDataManager)
}

public class JsonDataManager: NSObject {
    //Our jsonDataManager delegate
    weak var delegate: JsonDataManagerDelegate?
    
    //JSON Structure based on https://sverigesradio.se/api/documentation/v2/metoder/program.html
    struct programs: Codable {
        let copyright: String?
        let programs: [program]?
        let pagination: pagination?
    }
    
    struct program: Codable {
        let description: String?
        let email: String?
        let phone: String?
        let programur: String?
        let programslug: String?
        let programimage: String?
        let programimagetemplate: String?
        let programimagewide: String?
        let programimagetemplatewide: String?
        let socialimage: String?
        let socialimagetemplate: String?
        let socialmediaplatforms: [socialmediaplatforms]?
        let channel: channel?
        let archived: Bool?
        let hasondemand: Bool?
        let haspod: Bool?
        let responsibleeditor: String?
        let id: Int?
        let name: String?
    }
    
    struct socialmediaplatforms: Codable {
        let platform: String?
        let platformurl: String?
    }
    
    struct channel: Codable {
        let id: Int?
        let name: String?
    }
    
    struct pagination: Codable {
        let page: Int?
        let size: Int?
        let totalhits: Int?
        let totalpages: Int?
        let nextpage: String?
        let previouspage: String?
    }
    
    
    //Main entry URL
    let jsonUrlString = "https://api.sr.se/api/v2/programs?format=json"
    //Bool to track if we are handling a request currently
    var pendingRequest: Bool!
    //Store on what page we are
    var pagination: pagination?
    //Array to store our programs in
    let dataArray: NSMutableArray!
    //Decoder to decode our JSON Data
    let decoder: JSONDecoder!
    
    override public init() {
        //init the decoder that will used to decode our JSON files
        decoder = JSONDecoder()
        //init our array that will store the programs data
        dataArray = NSMutableArray.init()
        //set pending request to false in the begining
        pendingRequest = false
    }
    
    func requestData() {
        if self.pendingRequest == true {
            return
        }
        guard let url = URL(string: jsonUrlString) else {
            return
        }
        
        //flag that we are doing a request
        pendingRequest = true
        //send the request
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //remove pending request flag
            self.pendingRequest = false
            //check if our data is valid
            guard let data = data else {
                return
            }
            
            self.decodeJson(data: data)
            }.resume()
    }
    
    func requestNextData() {
        if self.pendingRequest == true {
            return
        }
        
        //Check if we have a valid next URL
        guard let urlString = self.pagination?.nextpage else {
            return
        }
        //Create our URL from string and check that it's valid
        guard let url = URL(string: urlString) else {
            return
        }
        //flag that we are doing a requestc§
        self.pendingRequest = true
        //send the request
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //remove pending request flag
            self.pendingRequest = false
            //check if our data is valid
            guard let data = data else {
                return
            }
            
            self.decodeJson(data: data)
            }.resume()
    }
    
    func decodeJson(data: Data) {
        
        do {
            let jsonPrograms =  try decoder.decode(programs.self, from: data)
            
            if let programArray = jsonPrograms.programs {
                //store our objects in our data array
                self.dataArray.addObjects(from: programArray)
            }
            
            if let page = jsonPrograms.pagination {
                //store our page where we are so we have the options to get the next page
                self.pagination = page
            }
            
            guard let delegate = self.delegate else {
                return
            }
            
            delegate.jsonDataRequestDidFinish(self)
        } catch {
            // TODO: handle error
            print("error \(error.localizedDescription)")
        }
    }
    
}
