//
//  ImageCacheManager.swift
//  Sveriges Radios
//
//  Created by Maxim Frisk on 2018-11-10.
//  Copyright © 2018 Maxim Frisk. All rights reserved.
//

import Foundation
import UIKit

class ImageCacheManager {
    //Singelton
    static let shared = ImageCacheManager()
    //Url Session
    let session = URLSession(configuration: .default)
    //Completion handler that returns and image
    typealias completionHandler = (UIImage?, URLResponse?, Error?) -> Void
    //List of tasks and their lists of completionHandlers
    var tasksList = [URL: [completionHandler]]()
    //Image cache
    let imageCache = NSCache<AnyObject, AnyObject>()
    
    func imageDataTask(with urlString: String, completion: @escaping completionHandler) {
        //if the image is already cached we just return it
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            completion(imageFromCache, nil, nil)
            return
        }
        
        //Create our URL from string and check that it's valid
        guard let url = URL(string: urlString) else {
            return
        }
        //if we are already started a task for the url we just append our completionHandler to the list of handlers to trigger.
        if tasksList.keys.contains(url) {
            tasksList[url]?.append(completion)
        } else {
            //Add our Url and handler to the list
            tasksList[url] = [completion]
            
            let _ = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
                DispatchQueue.main.async {
                    //Check that our data is not nil
                    guard let imageData = data else {
                        return
                    }
                    //store image in our cache
                    let imageToCache = UIImage(data: imageData)
                    self!.imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
                    
                    //Check our completion handler
                    guard let completionHandlers = self?.tasksList[url] else {
                        return
                    }
                    //Callback
                    for handler in completionHandlers {
                        handler(UIImage.init(data: imageData), response, error)
                    }
                    //we should clear our list of listed handlers. the rest should be taken care of the NSCache if we request the url again.
                    self?.tasksList.removeValue(forKey: url)
                }
            }).resume()
        }
    }
}
