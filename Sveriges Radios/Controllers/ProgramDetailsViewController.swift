//
//  ViewController.swift
//  Sveriges Radios
//
//  Created by Maxim Frisk on 2018-11-10.
//  Copyright © 2018 Maxim Frisk. All rights reserved.
//

import UIKit

class ProgramDetailsViewController: UIViewController {

    @IBOutlet var programImage: UIImageView!
    @IBOutlet var programTitle: UILabel!
    @IBOutlet var programDescription: UILabel!
    @IBOutlet var channelName: UILabel!
    
    public var program: JsonDataManager.program?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup our title
        if let title = self.program?.name {
            self.programTitle.text = title
        }
        //Setup description
        if let description = self.program?.description {
            self.programDescription.text = description
            self.programDescription.sizeToFit()
        }
        //Setup channel name
        if let channelName = self.program?.channel?.name {
            self.channelName.text = String.init(format: "kannal: %@", channelName)
        }
        //load and cache program wide image
        if let imageUrl = self.program?.programimagewide {
            ImageCacheManager.shared.imageDataTask(with: imageUrl) { (image, response, error) in
                self.programImage.image = image
            }
        } else {
            //Placeholder image fallback if url is not valid
            self.programImage.image = UIImage.init(named: "placeholder")
        }
    }

}

