//
//  TableViewController.swift
//  Sveriges Radios
//
//  Created by Maxim Frisk on 2018-11-10.
//  Copyright © 2018 Maxim Frisk. All rights reserved.
//

import UIKit

class ProgramTableViewController: UITableViewController {

    //Table cell reusable identifier
    let cellReuseIdentifier = "programCell"
    //Segue identifier
    let cellSegue = "detailsSegue"
    
    var dataArray : NSMutableArray!
    var jsonDataManager : JsonDataManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //register our cell with our identifier
        self.tableView.register(UINib(nibName: "ProgramTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.navigationItem.title = "Programs"

        //Get our initial data
        jsonDataManager = JsonDataManager.init()
        jsonDataManager.delegate = self
        jsonDataManager.requestData()
    }

    //MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //check if our array is valid and use it's count for amount of entries in the table
        if let array = dataArray {
            return array.count
        }
        //default to zero if array is nill
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Get a reuseale cell
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ProgramTableViewCell
        //Get our data by index
        let program = dataArray.object(at: indexPath.item) as! JsonDataManager.program
        //Assign our data to the cell
        cell.setupCell(program: program)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //let's increase our default size a bit
        return 75.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Handle clickss on cells and trigger a segue.
        performSegue(withIdentifier: cellSegue, sender: tableView.cellForRow(at: indexPath))
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.tableView.numberOfRows(inSection: 0)-1
        //check if we have scrolled to bottom of the table
        if indexPath.row == lastElement {
            //Request more data into tableview
            jsonDataManager.requestNextData()
            
            //Create a loading indicator to inform we are loading more date at the bottom
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            //use the tableview footer to display our spinner and unhide it
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
        }
    }

    
    //MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Get the new view controller using segue.destination.
        //Pass the selected object to the new view controller.
        if segue.identifier == cellSegue {
            guard let program = (sender as! ProgramTableViewCell).program else {
                return
            }
            
            let destinationController = segue.destination as! ProgramDetailsViewController
            destinationController.program = program
            
        }
    }
}

extension ProgramTableViewController: JsonDataManagerDelegate {
    
    func jsonDataRequestDidFinish(_ dataManager: JsonDataManager) {
        if dataManager.dataArray != nil {
            //Since this will be running from a background thread we need to dispatch this on the main thread.
            DispatchQueue.main.async {
                //setting data and reloading table
                self.dataArray = dataManager.dataArray
                self.tableView.reloadData()
            }
        }
    }
}
