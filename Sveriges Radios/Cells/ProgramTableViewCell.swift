//
//  ProgramTableViewCell.swift
//  Sveriges Radios
//
//  Created by Maxim Frisk on 2018-11-10.
//  Copyright © 2018 Maxim Frisk. All rights reserved.
//

import UIKit

class ProgramTableViewCell: UITableViewCell {

    @IBOutlet weak var programImage: UIImageView!
    @IBOutlet weak var programTitle: UILabel!
    @IBOutlet weak var programDescription: UILabel!
    
    var program: JsonDataManager.program?
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        programTitle.text = ""
        programDescription.text = ""
        programImage.image = UIImage.init(named: "placeholder")
    }
    
    public func setupCell(program: JsonDataManager.program) {
        self.program = program
        
        if let title = program.name {
            programTitle.text = title
        }
        
        if let description = program.description {
            programDescription.text = description
        }

        if let imageUrl = program.programimage {
            ImageCacheManager.shared.imageDataTask(with: imageUrl) { (image, response, error) in
                //check if the cell still has a valid programimage URL after the completion returns.
                if let currentUrl = self.program?.programimage {
                    //only set the image if the cell still has the same url as the requested one, not the most beautiful solution but works for now.
                    if imageUrl == currentUrl {
                        self.programImage.image = image
                    }
                }
            }
        } else {
            programImage.image = UIImage.init(named: "placeholder")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //make sure we reset this image to the placeholder one when we reuse it.
        programImage.image = UIImage.init(named: "placeholder")
    }
    
}
